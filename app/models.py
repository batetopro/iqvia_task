from datetime import datetime
from app import db


class Email(db.Model):
    __tablename__ = 'emails'
    id = db.Column(db.Integer, primary_key=True)
    contact_id = db.Column(db.Integer, db.ForeignKey('contacts.id'))
    value = db.Column(db.String(140), nullable=False)


class Contact(db.Model):
    __tablename__ = 'contacts'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True, nullable=False)
    first_name = db.Column(db.String(64), nullable=False)
    last_name = db.Column(db.String(64), nullable=False)
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    emails = db.relationship('Email', backref='owner', lazy='dynamic')

    def __repr__(self):
        return '<Contact {0}: {1} {2}>'.format(self.username, self.first_name, self.last_name)

    @staticmethod
    def get_by_username(username):
        return Contact.query.filter_by(username=username).first()

    def assign_email(self, value):
        if self.has_email(value):
            return

        email = Email(value=value, contact_id=self.id)

        db.session.add(email)
        db.session.commit()

    def remove_email(self, value):
        if not self.has_email(value):
            return

        Email.query.filter_by(value=value, contact_id=self.id).delete()
        db.session.commit()

    def has_email(self, value):
        for email in self.emails:
            if email.value == value:
                return True
        return False

    def to_json(self):
        result = dict()
        result['username'] = self.username
        result['first_name'] = self.first_name
        result['last_name'] = self.last_name
        return result
