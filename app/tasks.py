import uuid
import time
from datetime import datetime, timedelta
from app import celery, Contact, db


@celery.task()
def create_random():

    while True:
        parts = uuid.uuid4().__str__().split('-')

        contact = Contact(
            username=parts[0],
            first_name='{}-{}'.format(parts[1], parts[2]),
            last_name='{}-{}'.format(parts[3], parts[4])
        )

        db.session.add(contact)
        db.session.commit()
        time.sleep(15)


@celery.task()
def delete_old():
    while True:
        time.sleep(15)
        since = datetime.utcnow() - timedelta(minutes=1)
        for c in Contact.query.filter(Contact.timestamp <= since):
            for e in c.emails:
                c.remove_email(e.value)
            db.session.delete(c)
            db.session.commit()
