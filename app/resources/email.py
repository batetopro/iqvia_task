from flask_restful import reqparse, Resource
from ..models import db, Contact


class EmailResource(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument('value')

    def put(self, username):
        args = self.parser.parse_args()

        contact = Contact.get_by_username(username)
        if contact is None:
            return {'message': 'Contact could not be found'}, 404

        if contact.has_email(args['value']):
            return {'message': 'E-mail is already linked'}, 400

        contact.assign_email(args['value'])

        return {'message': 'E-mail address was added.'}

    def delete(self, username):
        args = self.parser.parse_args()

        contact = Contact.get_by_username(username)
        if contact is None:
            return {'message': 'Contact could not be found'}, 404

        if not contact.has_email(args['value']):
            return {'message': 'E-mail could not be found'}, 404

        contact.remove_email(args['value'])

        return {'message': 'E=mail address was removed.'}
