from flask_restful import reqparse, Resource
from ..models import db, Contact


class ContactList(Resource):
    def get(self):
        result = []

        for contact in Contact.query.all():
            result.append(contact.to_json())

        return result


class ContactResource(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument('first_name')
    parser.add_argument('last_name')

    def get(self, username):
        contact = Contact.get_by_username(username)
        if contact is None:
            return {'message': 'Contact could not be found'}, 404

        result = contact.to_json()
        result['emails'] = [email.value for email in contact.emails]

        return result

    def put(self, username):
        args = self.parser.parse_args()
        contact = Contact.get_by_username(username)
        if contact is None:
            contact = Contact(username=username)
        contact.first_name = args['first_name']
        contact.last_name = args['last_name']
        db.session.add(contact)
        db.session.commit()
        return {'message': 'Contact was saved.', 'contact': contact.to_json()}

    def delete(self, username):
        contact = Contact.get_by_username(username)
        if contact is None:
            return {'message': 'Contact could not be found'}, 404

        for email in contact.emails:
            contact.remove_email(email.value)

        db.session.delete(contact)
        db.session.commit()
        return {'message': 'Contact was deleted.', 'contact': contact.to_json()}
