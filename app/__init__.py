from flask import Flask
from celery import Celery
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_restful import Api
from .settings import Settings


app = Flask(__name__)

app.config.from_object(Settings)
db = SQLAlchemy(app)
migrate = Migrate(app, db)
api = Api(app)

celery = Celery(app.name, broker=app.config['CELERY_BROKER_URL'])
celery.conf.update(app.config)

from .resources import ContactList, ContactResource, EmailResource

api.add_resource(ContactList, '/contacts')
api.add_resource(ContactResource, '/contact/<username>')
api.add_resource(EmailResource, '/email/<username>')

from app.models import Contact

from .tasks import create_random, delete_old
# create_random.apply_async()
# delete_old.apply_async()
