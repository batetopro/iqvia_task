"""contact created

Revision ID: f69c6ff9aae5
Revises: e3bf5f35f616
Create Date: 2020-03-02 22:17:04.671329

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'f69c6ff9aae5'
down_revision = 'e3bf5f35f616'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('contacts', sa.Column('timestamp', sa.DateTime(), nullable=True))
    op.create_index(op.f('ix_contacts_timestamp'), 'contacts', ['timestamp'], unique=False)
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_index(op.f('ix_contacts_timestamp'), table_name='contacts')
    op.drop_column('contacts', 'timestamp')
    # ### end Alembic commands ###
