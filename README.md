# Contacts

The goal of this exercise is to build a fully functional API application using best
practices and reusable code.

### Endpoints

* `GET` /contacts - List all contacts
* `GET` /contact/<username> - Find a contact by username
* `PUT` /contact/<username> - Create a new contact or update an existing one
* `DELETE` /contact/<username> - Delete a contact
* `PUT` /email/<username> - Adds an email to a contact
* `DELETE` /email/<username> - Removes an email from a contact


### Installation

Prepare virtual environment
```bash
sudo apt-get install python3-pip
sudo pip3 install virtualenv 
virtualenv venv 
source venv/bin/activate
pip install -r requirements.txt
```

To setup connections edit the file app/settings.py:


* SQLALCHEMY_DATABASE_URI - connection to database
* CELERY_BROKER_URL - connection to Redis
* CELERY_RESULT_BACKEND - connection to Redis


To 'install' the database schema run this command:
```bash
flask db upgrade
```

### Run

To run the application:
```bash
flask run
```

To run the Postman collection, import into Postman the following collection:
```bash
contacts.postman_collection.json
```

To run the Flinstones contacts seeder run:
```bash
python3 seed.py
```

To run a celery worker run:
```bash
celery worker -A app.celery --loglevel=info
```

To enable the celery background tasks, uncomment the following lines in `app/__init__.py`:
```
# create_random.apply_async()
# delete_old.apply_async()
```