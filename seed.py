from app import db, Contact

fred = Contact(username='fred', first_name="Fred", last_name="Flintstone")
wilma = Contact(username='wilma', first_name="Wilma", last_name="Flintstone")
pebbles = Contact(username='pebbles', first_name="Pebbles", last_name="Flintstone")
barney = Contact(username='barney', first_name="Barney", last_name="Rubble")
betty = Contact(username='betty', first_name="Betty", last_name="Rubble")
bamm = Contact(username='bamm', first_name="Bamm Bamm", last_name="Rubble")

db.session.add(fred)
db.session.add(wilma)
db.session.add(pebbles)
db.session.add(barney)
db.session.add(betty)
db.session.add(bamm)
db.session.commit()
